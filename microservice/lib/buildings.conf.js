"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.canoas = exports.centro = void 0;
var centro = {
  predio1: {
    name: "Prédio 1",
    source: "http://salas.ufcspa.edu.br/w8_relatorio_dia.php?vid_pd=1&vid_instituicao=1"
  },
  predio2: {
    name: "Prédio 2",
    source: "http://salas.ufcspa.edu.br/w8_relatorio_dia.php?vid_pd=2&vid_instituicao=1"
  },
  predio3: {
    name: "Prédio 3",
    source: "http://salas.ufcspa.edu.br/w8_relatorio_dia.php?vid_pd=3&vid_instituicao=1"
  }
};
exports.centro = centro;
var canoas = {
  predio1: {
    name: "Predio 1",
    sources: []
  }
};
exports.canoas = canoas;