"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _keenTracking = _interopRequireDefault(require("keen-tracking"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var client = new _keenTracking.default({
  projectId: '5abe99edc9e77c0001b46506',
  writeKey: 'ED1DA720717F40F76E99A7363605DDBD2C1852BAA1C19E08A2CC70E09F83DAC2710E7122161EEA63172FCADB4C6C59620D264E10F10194E86CD2B24C609F12DBC9425743A3724B0B8ED990AD63FB577F9D2B13B278EA0DD4F2D1BF2A94E75005'
});

var KeenioProvider =
/*#__PURE__*/
function () {
  function KeenioProvider() {
    _classCallCheck(this, KeenioProvider);
  }

  _createClass(KeenioProvider, null, [{
    key: "save",
    value: function save(eventName, content) {
      client.recordEvent(eventName, content);
    }
  }]);

  return KeenioProvider;
}();

exports.default = KeenioProvider;