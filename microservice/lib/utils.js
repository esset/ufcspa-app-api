"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.normalize = normalize;

function normalize(val) {
  //Remove accents/diacritics in a string and trim
  return val.normalize('NFD').replace(/[\u0300-\u036f]/g, "").trim();
}