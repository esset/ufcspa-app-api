"use strict";

var _requestPromise = _interopRequireDefault(require("request-promise"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Function that implements request-promise to do a request to a URI with some options
 * @param link - The URI to send the request
 * @param {object} [options] - Options to be passed with the request
 * @returns {Promise}
 */
module.exports.requester = function (link) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  options.uri = link;
  return (0, _requestPromise.default)(options);
};