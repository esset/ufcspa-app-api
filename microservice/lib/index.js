"use strict";

require("@babel/polyfill");

var userAgent = _interopRequireWildcard(require("express-useragent"));

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var _lodash = require("lodash");

var _microrouter = require("microrouter");

var _activity = require("./activity");

var _keenioProvider = _interopRequireDefault(require("./keenioProvider"));

var _buildings = require("./buildings.conf");

var _utils = require("./utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var cors = require('micro-cors')();

module.exports = cors((0, _microrouter.router)( // TODO: Use reverse proxy to remove V2 prefix
(0, _microrouter.get)('/v2', function (req, res) {
  console.log("Fired route: GET: /");
  res.end('V2 that searches in the page w8_relatorio_dia.php');
}), (0, _microrouter.get)('/v2/activities',
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(req, res) {
    var data;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            console.log("Fired route: GET: /activities by:", req.headers["X-Forwarded-For"] || req.connection.remoteAddress);

            _keenioProvider.default.save('userAgents', userAgent.parse(req.headers['user-agent']));

            _context.t0 = req.query.campus;
            _context.next = _context.t0 === "canoas" ? 5 : 9;
            break;

          case 5:
            _context.next = 7;
            return (0, _activity.converter)(_buildings.canoas, req.query);

          case 7:
            data = _context.sent;
            return _context.abrupt("break", 12);

          case 9:
            _context.next = 11;
            return (0, _activity.converter)(_buildings.centro, req.query);

          case 11:
            data = _context.sent;

          case 12:
            data = statusFilter(data, req.query.status);
            data = searchFilter(data, req.query.search); // Set the consulted time with timezone Sao Paulo since UFCSPA is under this timezone

            data.consultedAt = (0, _momentTimezone.default)().tz('America/Sao_Paulo').format(); // Set the URL that generated this data

            data.url = "/v2/activities"; // Return all the activities separated by building

            res.writeHead(200, {
              "Content-Type": "application/json"
            });
            res.end(JSON.stringify(data));

          case 18:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}())));

function statusFilter(data) {
  var status = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "all";

  (function () {
    switch (status) {
      case "all":
        break;

      case "ongoing":
        var now = (0, _momentTimezone.default)().format('x');

        for (var i = 0; i < data.buildings.length; i++) {
          (0, _lodash.remove)(data.buildings[i].activities, function (activity) {
            return activity.hourBegin <= now && activity.hourEnd >= now ? false : true;
          });
          data.buildings[i].metadata.numActivities = data.buildings[i].activities.length;
        }

        break;

      case "moment":
      default:
        var _loop = function _loop(_i) {
          var start = (0, _momentTimezone.default)().subtract(15, 'minutes').format('x'); //Show activities that already started with a tolerance of 15 minutes

          var end = (0, _momentTimezone.default)().add(30, 'minutes').format('x'); //Show activities that will begin within 30 minutes

          (0, _lodash.remove)(data.buildings[_i].activities, function (activity) {
            return activity.hourBegin >= start && activity.hourBegin <= end ? false : true;
          });
          data.buildings[_i].metadata.numActivities = data.buildings[_i].activities.length;
        };

        for (var _i = 0; _i < data.buildings.length; _i++) {
          _loop(_i);
        }

        break;
    }
  })();

  return data;
}

function searchFilter(data) {
  var query = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
  if (query == "") return data;

  for (var i = 0; i < data.buildings.length; i++) {
    data.buildings[i].activities = data.buildings[i].activities.filter(function (activity) {
      var matchName = (0, _utils.normalize)(activity.name).toLowerCase().indexOf((0, _utils.normalize)(query).toLowerCase()) > -1;
      var matchCourseName = (0, _utils.normalize)(activity.course.name).toLowerCase().indexOf((0, _utils.normalize)(query).toLowerCase()) > -1;
      return matchName || matchCourseName;
    });
  }

  return data;
}