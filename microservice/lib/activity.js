"use strict";

var _cheerio = _interopRequireDefault(require("cheerio"));

var _requester = require("./requester");

var _lodash = require("lodash");

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//https://stackoverflow.com/questions/20672237/can-i-add-more-jquery-selectors-to-cheerio-node-js

/**
 * Return all the activities of a buildings object
 * @param { Object } buildings
 * @returns { Object }
 */
module.exports.converter =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(buildings) {
    var data, i, _loop, buildingName;

    return regeneratorRuntime.wrap(function _callee$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            data = {
              buildings: []
            };
            i = 0;
            _loop =
            /*#__PURE__*/
            regeneratorRuntime.mark(function _loop(buildingName) {
              var source, activities;
              return regeneratorRuntime.wrap(function _loop$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      //Iterate over all the registered buildings
                      source = buildings[buildingName].source; //For every building, get the source that has information about it

                      data.buildings[i] = {
                        name: buildings[buildingName].name,
                        metadata: {},
                        activities: []
                      };
                      console.log("Source:", source);
                      _context.next = 5;
                      return parser(source);

                    case 5:
                      activities = _context.sent;
                      console.log("Tried TV:", source, "Num Results:", activities.length); //Add the the activities of that page to the array of activities of that building

                      data.buildings[i].activities = data.buildings[i].activities.concat(activities);
                      (0, _lodash.map)(data.buildings[i].activities, function (activity) {
                        activity.place.building = buildings[buildingName].name;
                      }); //For metadata or statistical purposes

                      data.buildings[i].metadata.numActivities = data.buildings[i].activities.length;
                      i++;

                    case 11:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _loop, this);
            });
            _context2.t0 = regeneratorRuntime.keys(buildings);

          case 4:
            if ((_context2.t1 = _context2.t0()).done) {
              _context2.next = 9;
              break;
            }

            buildingName = _context2.t1.value;
            return _context2.delegateYield(_loop(buildingName), "t2", 7);

          case 7:
            _context2.next = 4;
            break;

          case 9:
            return _context2.abrupt("return", data);

          case 10:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();
/**
 * Parser with cheerio the HTML of a link. Get all the activities of that source
 * @param {String} link
 * @param {Object} options
 * @returns {Promise.<Array>}
 */


function parser(_x2) {
  return _parser.apply(this, arguments);
}
/**
 * Get the basic information of a activity
 * @param {cheerio} $
 * @param {String} rawSubject - The HTML that will be used as scope by cheerio
 * @returns {{}}
 */


function _parser() {
  _parser = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(link) {
    var options,
        $,
        activities,
        _args3 = arguments;
    return regeneratorRuntime.wrap(function _callee2$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            options = _args3.length > 1 && _args3[1] !== undefined ? _args3[1] : {};
            //TODO: Use @essetwide/html-scrapper
            Object.assign(options, {
              transform: function transform(body) {
                return _cheerio.default.load(body);
              }
            });
            _context3.prev = 2;
            _context3.next = 5;
            return (0, _requester.requester)(link, options);

          case 5:
            $ = _context3.sent;
            activities = [];
            $('.selected:has(td)').map(function (i, rawSubject) {
              var activity;
              activity = getLesson($, rawSubject);
              activity.place = getPlace($, rawSubject);
              activity.course = getCourse($, rawSubject);
              activities.push(activity);
            });
            activities.splice(0, 1); //The "header" in HTML has a .selected class to, so remove it

            return _context3.abrupt("return", activities);

          case 12:
            _context3.prev = 12;
            _context3.t0 = _context3["catch"](2);

            if (!(_context3.t0.name == "RequestError")) {
              _context3.next = 17;
              break;
            }

            console.error("ERROR activity.js - parser::RequestError on", link);
            return _context3.abrupt("return", []);

          case 17:
            console.log("ERROR activity.js - parser:");
            console.log(_context3.t0);

          case 19:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee2, this, [[2, 12]]);
  }));
  return _parser.apply(this, arguments);
}

function getLesson($, rawSubject) {
  var activity = {};
  activity.name = $('td:first-of-type big > strong', rawSubject).text();
  activity.description = $('td:first-of-type > span > small', rawSubject).text();
  activity.hourLabel = $('td:nth-of-type(3)', rawSubject).text(); // Convert de Hour Lavel to an Array

  var hourBeginArray = activity.hourLabel.substring(0, activity.hourLabel.indexOf('-')).trim().split(":");
  var hourEndArray = activity.hourLabel.substring(activity.hourLabel.indexOf('-') + 1, activity.hourLabel.length).trim().split(":"); //
  //Use the Array of hours to get the BEGIN timestamp (UTC) and END timestamp (UTC)

  activity.hourBegin = _momentTimezone.default.tz({
    hour: parseInt(hourBeginArray[0]),
    minute: parseInt(hourBeginArray[1]),
    second: 0,
    millisecond: 0
  }, 'America/Sao_Paulo').format('x');
  activity.hourEnd = _momentTimezone.default.tz({
    hour: parseInt(hourEndArray[0]),
    minute: parseInt(hourEndArray[1]),
    second: 0,
    millisecond: 0
  }, 'America/Sao_Paulo').format('x');
  return activity;
}
/**
 * @typedef {Object} Place
 * @property {number} roomNumber The number of the room
 * @property {String} building The name of the building
 * @property {String} description An optional description of that room
 */

/**
 * Get the place where a activity will occur
 * @param {cheerio} $
 * @param {String} rawSubject - The HTML that will be used as scope by cheerio
 * @returns {Place}
 */


function getPlace($, rawSubject) {
  var place = {};
  var rawBuilding = $('td:nth-of-type(4)', rawSubject).clone() //clone the element
  .children() //select all the children
  .remove() //remove all the children
  .end() //again go back to selected element
  .text(); //get the text of element

  place.roomNumber = $('td:nth-of-type(4) strong', rawSubject).text();
  place.building = rawBuilding.substring(3);
  place.description = $('td:nth-of-type(4) small', rawSubject).text();
  return place;
}
/**
 * Get the course of a activity
 * @param {cheerio} $
 * @param {String} rawSubject - The HTML that will be used as scope by cheerio
 * @returns {{}}
 */


function getCourse($, rawSubject) {
  var course = {};
  course.name = $('td:nth-of-type(2)', rawSubject).clone() //clone the element
  .children() //select all the children
  .remove() //remove all the children
  .end() //again go back to selected element
  .text(); //get the text of element

  course.description = $('td:nth-of-type(2) small', rawSubject).text();
  return course;
}