"use strict";

var parser = function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(link, options) {
        var $, subjects;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:

                        Object.assign(options, {
                            transform: function transform(body) {
                                return _cheerio2.default.load(body);
                            }
                        });

                        _context2.prev = 1;
                        _context2.next = 4;
                        return (0, _requester.requester)(link, options);

                    case 4:
                        $ = _context2.sent;
                        subjects = [];


                        $('.selected').map(function (i, rawSubject) {
                            var subject = void 0;
                            subject = getSubject($, rawSubject);
                            subject.place = getPlace($, rawSubject);
                            subject.course = getCourse($, rawSubject);
                            subjects.push(subject);
                        });

                        subjects.splice(0, 1); //The "header" in HTML has a .selected class to, so remove it
                        subjects.splice(-1, 1); //The HTML has an empty tag with .selected class at the end, so remove it

                        return _context2.abrupt("return", subjects);

                    case 12:
                        _context2.prev = 12;
                        _context2.t0 = _context2["catch"](1);

                        console.log("SUBJECT-CONVERSOR CONVERSER():: ERROR");
                        console.log(_context2.t0);

                    case 16:
                    case "end":
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[1, 12]]);
    }));

    return function parser(_x2, _x3) {
        return _ref2.apply(this, arguments);
    };
}();

var _cheerio = require("cheerio");

var _cheerio2 = _interopRequireDefault(_cheerio);

var _requester = require("./requester");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

//https://stackoverflow.com/questions/20672237/can-i-add-more-jquery-selectors-to-cheerio-node-js

module.exports.converser = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(buildings) {
        var data, buildingName, televisions, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, television, i, subjects;

        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        data = {};
                        _context.t0 = regeneratorRuntime.keys(buildings);

                    case 2:
                        if ((_context.t1 = _context.t0()).done) {
                            _context.next = 42;
                            break;
                        }

                        buildingName = _context.t1.value;
                        //Iterate over all the registered buildings
                        televisions = buildings[buildingName]; //For every building, get the televisions that has information about it

                        data[buildingName] = { subjects: [] };
                        _iteratorNormalCompletion = true;
                        _didIteratorError = false;
                        _iteratorError = undefined;
                        _context.prev = 9;
                        _iterator = televisions[Symbol.iterator]();

                    case 11:
                        if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                            _context.next = 26;
                            break;
                        }

                        television = _step.value;
                        //For every television...
                        i = 1;

                    case 14:
                        _context.next = 16;
                        return parser(television, { qs: { vpagina: i++ } });

                    case 16:
                        subjects = _context.sent;

                        console.log("Tried TV:", television, "Page:", i - 1, "Num Results:", subjects.length);

                        if (!(subjects.length == 0)) {
                            _context.next = 20;
                            break;
                        }

                        return _context.abrupt("break", 23);

                    case 20:
                        //Add the the subjects of that page to the array of subjects of that building
                        data[buildingName].subjects = data[buildingName].subjects.concat(subjects);

                        return _context.abrupt("break", 23);

                    case 22:
                        if (true) {
                            _context.next = 14;
                            break;
                        }

                    case 23:
                        _iteratorNormalCompletion = true;
                        _context.next = 11;
                        break;

                    case 26:
                        _context.next = 32;
                        break;

                    case 28:
                        _context.prev = 28;
                        _context.t2 = _context["catch"](9);
                        _didIteratorError = true;
                        _iteratorError = _context.t2;

                    case 32:
                        _context.prev = 32;
                        _context.prev = 33;

                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }

                    case 35:
                        _context.prev = 35;

                        if (!_didIteratorError) {
                            _context.next = 38;
                            break;
                        }

                        throw _iteratorError;

                    case 38:
                        return _context.finish(35);

                    case 39:
                        return _context.finish(32);

                    case 40:
                        _context.next = 2;
                        break;

                    case 42:
                        return _context.abrupt("return", data);

                    case 43:
                    case "end":
                        return _context.stop();
                }
            }
        }, _callee, undefined, [[9, 28, 32, 40], [33,, 35, 39]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

function getSubject($, rawSubject) {
    var subject = {};

    subject.name = $('td:first-of-type big > strong', rawSubject).text();
    subject.description = $('td:first-of-type > span > strong', rawSubject).text();
    subject.hour = $('td:nth-of-type(3)', rawSubject).text();

    return subject;
}

function getPlace($, rawSubject) {
    var place = {};
    var rawBuilding = $('td:nth-of-type(4)', rawSubject).clone() //clone the element
    .children() //select all the children
    .remove() //remove all the children
    .end() //again go back to selected element
    .text(); //get the text of element
    place.roomNumber = $('td:nth-of-type(4) strong', rawSubject).text();
    place.building = rawBuilding.substring(3);
    place.description = $('td:nth-of-type(4) small', rawSubject).text();

    return place;
}

function getCourse($, rawSubject) {
    var course = {};

    course.name = $('td:nth-of-type(2)', rawSubject).clone() //clone the element
    .children() //select all the children
    .remove() //remove all the children
    .end() //again go back to selected element
    .text(); //get the text of element
    course.description = $('td:nth-of-type(2) small', rawSubject).text();

    return course;
}