import KeenTracking from 'keen-tracking';

var client = new KeenTracking({
  projectId: '5abe99edc9e77c0001b46506',
  writeKey: 'ED1DA720717F40F76E99A7363605DDBD2C1852BAA1C19E08A2CC70E09F83DAC2710E7122161EEA63172FCADB4C6C59620D264E10F10194E86CD2B24C609F12DBC9425743A3724B0B8ED990AD63FB577F9D2B13B278EA0DD4F2D1BF2A94E75005'
});

export default class KeenioProvider {
  static save(eventName, content) {
    client.recordEvent(eventName, content);
  }
}
