import rp from "request-promise";

/**
 * Function that implements request-promise to do a request to a URI with some options
 * @param link - The URI to send the request
 * @param {object} [options] - Options to be passed with the request
 * @returns {Promise}
 */
module.exports.requester = (link, options = {}) => {
    options.uri = link;
    return rp(options);
};