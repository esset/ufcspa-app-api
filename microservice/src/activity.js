import cheerio from 'cheerio';
import { requester } from "./requester";
import { map } from "lodash";
import moment from 'moment-timezone';

//https://stackoverflow.com/questions/20672237/can-i-add-more-jquery-selectors-to-cheerio-node-js

/**
 * Return all the activities of a buildings object
 * @param { Object } buildings
 * @returns { Object }
 */
module.exports.converter = async (buildings) => {
  const data = { buildings: [] };
  let i = 0;
  for (let buildingName in buildings) { //Iterate over all the registered buildings

    const source = buildings[buildingName].source; //For every building, get the source that has information about it

    data.buildings[i] = { name: buildings[buildingName].name,  metadata: {}, activities: [] };
    console.log("Source:", source);
    const activities = await parser(source);
    console.log("Tried TV:", source, "Num Results:", activities.length);

    //Add the the activities of that page to the array of activities of that building
    data.buildings[i].activities = data.buildings[i].activities.concat(activities);

    map(data.buildings[i].activities, (activity) => {
      activity.place.building =  buildings[buildingName].name;
    });

    //For metadata or statistical purposes
    data.buildings[i].metadata.numActivities = data.buildings[i].activities.length;

    i++;
  }

  return data;
};

/**
 * Parser with cheerio the HTML of a link. Get all the activities of that source
 * @param {String} link
 * @param {Object} options
 * @returns {Promise.<Array>}
 */
async function parser(link, options = {}) {
    //TODO: Use @essetwide/html-scrapper
    Object.assign(options, {
        transform: function (body) {
          return cheerio.load(body);
        }
    });

    try {
        let $ = await requester(link, options);

        let activities = [];
        $('.selected:has(td)').map((i, rawSubject) => {
            let activity;
            activity = getLesson($, rawSubject);
            activity.place = getPlace($, rawSubject);
            activity.course = getCourse($, rawSubject);
            activities.push(activity);
        });

        activities.splice(0, 1); //The "header" in HTML has a .selected class to, so remove it

        return activities;

    } catch (e) {
        if (e.name == "RequestError"){
            console.error("ERROR activity.js - parser::RequestError on", link);
            return [];
        }

        console.log("ERROR activity.js - parser:");
        console.log(e);
    }
}

/**
 * Get the basic information of a activity
 * @param {cheerio} $
 * @param {String} rawSubject - The HTML that will be used as scope by cheerio
 * @returns {{}}
 */
function getLesson($, rawSubject){
    let activity = {};
    activity.name = $('td:first-of-type big > strong', rawSubject).text();
    activity.description = $('td:first-of-type > span > small', rawSubject).text();
    activity.hourLabel = $('td:nth-of-type(3)', rawSubject).text();

    // Convert de Hour Lavel to an Array
    let hourBeginArray = activity.hourLabel.substring(0, activity.hourLabel.indexOf('-')).trim().split(":");
    let hourEndArray = activity.hourLabel.substring((activity.hourLabel.indexOf('-')+1), activity.hourLabel.length).trim().split(":");
    //
    //Use the Array of hours to get the BEGIN timestamp (UTC) and END timestamp (UTC)
    activity.hourBegin =  moment.tz({ hour: parseInt(hourBeginArray[0]), minute: parseInt(hourBeginArray[1]), second:0, millisecond:0 }, 'America/Sao_Paulo').format('x');
    activity.hourEnd =  moment.tz({ hour: parseInt(hourEndArray[0]), minute: parseInt(hourEndArray[1]), second:0, millisecond:0 }, 'America/Sao_Paulo').format('x');
    return activity;
}

/**
 * @typedef {Object} Place
 * @property {number} roomNumber The number of the room
 * @property {String} building The name of the building
 * @property {String} description An optional description of that room
 */

/**
 * Get the place where a activity will occur
 * @param {cheerio} $
 * @param {String} rawSubject - The HTML that will be used as scope by cheerio
 * @returns {Place}
 */
function getPlace($, rawSubject) {
    let place = {};
    let rawBuilding =
        $('td:nth-of-type(4)', rawSubject).clone()	//clone the element
            .children()	//select all the children
            .remove()	//remove all the children
            .end()	//again go back to selected element
            .text();	//get the text of element
    place.roomNumber = $('td:nth-of-type(4) strong', rawSubject).text();
    place.building = rawBuilding.substring(3);
    place.description = $('td:nth-of-type(4) small', rawSubject).text();

    return place;
}

/**
 * Get the course of a activity
 * @param {cheerio} $
 * @param {String} rawSubject - The HTML that will be used as scope by cheerio
 * @returns {{}}
 */
function getCourse($, rawSubject) {
    let course = {};

    course.name =
        $('td:nth-of-type(2)', rawSubject).clone()	//clone the element
            .children()	//select all the children
            .remove()	//remove all the children
            .end()	//again go back to selected element
            .text();	//get the text of element
    course.description = $('td:nth-of-type(2) small', rawSubject).text();

    return course;
}
