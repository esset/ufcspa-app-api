import "@babel/polyfill";
import * as userAgent from "express-useragent";
const cors = require('micro-cors')();
import moment from 'moment-timezone';
import { remove, map } from "lodash";
import { router, get, post } from "microrouter";
import { converter } from "./activity"
import KeenioProvider from './keenioProvider';
import { centro, canoas } from "./buildings.conf"; //All the campi of UFCSPA
import { normalize } from "./utils"

module.exports = cors(router(
    // TODO: Use reverse proxy to remove V2 prefix
  get('/v2', (req, res) => {
     console.log("Fired route: GET: /");
     res.end('V2 that searches in the page w8_relatorio_dia.php');
  }),
  get('/v2/activities', async (req, res) => {

    console.log("Fired route: GET: /activities by:", req.headers["X-Forwarded-For"] || req.connection.remoteAddress);
    KeenioProvider.save('userAgents', userAgent.parse(req.headers['user-agent']));
    let data;
    switch(req.query.campus) {
      case "canoas":
        data = await converter(canoas, req.query);
        break;
      default:
        data = await converter(centro, req.query);
    }
	
	data = statusFilter(data, req.query.status);
  data= searchFilter(data, req.query.search);
	
    // Set the consulted time with timezone Sao Paulo since UFCSPA is under this timezone
    data.consultedAt = moment().tz('America/Sao_Paulo').format();

    // Set the URL that generated this data
    data.url = "/v2/activities";

    // Return all the activities separated by building
	res.writeHead(200, {"Content-Type": "application/json"});
    res.end(JSON.stringify(data));

  }),
));

function statusFilter(data, status = "all"){
	switch(status){
		case "all":
			break;
		case "ongoing":
			let now = moment().format('x');
			for(let i =0; i < data.buildings.length; i++){
	          remove(data.buildings[i].activities, (activity) => {
	            return (activity.hourBegin <= now && activity.hourEnd >= now) ? false : true;
	          });
	          data.buildings[i].metadata.numActivities = data.buildings[i].activities.length;
	        }
			break;
		case "moment":
		default:
			for(let i =0; i < data.buildings.length; i++){
	          let start = moment().subtract(15, 'minutes').format('x'); //Show activities that already started with a tolerance of 15 minutes
	          let end = moment().add(30, 'minutes').format('x'); //Show activities that will begin within 30 minutes

	          remove(data.buildings[i].activities, (activity) => {
	            return (activity.hourBegin >= start && activity.hourBegin <= end) ? false : true;
	          });

	          data.buildings[i].metadata.numActivities = data.buildings[i].activities.length;

            }
			break;
	}
	return data;
}

function searchFilter(data, query = ""){
  if(query == "")
    return data;

  for(let i =0; i < data.buildings.length; i++){
      data.buildings[i].activities = data.buildings[i].activities.filter((activity) => {

        let matchName = normalize(activity.name)
          .toLowerCase().indexOf(normalize(query).toLowerCase()) > -1;

        let matchCourseName = normalize(activity.course.name)
          .toLowerCase().indexOf(normalize(query).toLowerCase()) > -1;

        return (matchName || matchCourseName);
      });
    }
    return data;
}