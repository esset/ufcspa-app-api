import { requester } from "../src/requester";

test('shouldGetError', async () => {
  expect.assertions(1);
  try {
    await requester("dsaasd");
  } catch (e) {
    expect(e.message).toEqual('Error: Invalid URI "dsaasd"');
  }
});