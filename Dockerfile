FROM node:8

WORKDIR /opt/microservice
ADD microservice .

RUN npm install

RUN npm run build

EXPOSE 3000
CMD ["npm", "run",  "start"]
